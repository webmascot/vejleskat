---
id: 531caeeb-eaf3-4c44-91bb-d44c87c9bb1f
blueprint: kontakt
title: 'Kontakt os'
personer:
  -
    image: content/kai.jpg
    navn: 'Kai A. Petersen'
    telefon: '20218757'
    email: kai@vejleskat.dk
    type: person
    enabled: true
  -
    image: content/lise.jpg
    navn: 'Lise Graversen'
    telefon: '20218757'
    email: lise@vejleskat.dk
    type: person
    enabled: true
updated_by: 2abdf0b4-7093-453b-81b8-b442c1efabfa
updated_at: 1658322296
template: kontakt
seotamic_title: title
seotamic_title_prepend: true
seotamic_title_append: true
seotamic_meta_description: custom
seotamic_custom_meta_description: 'Vejle Skatterådgivning- & Erhvervssupport har eksisteret i mere end 28 år. Vi har derfor stor erfaring indenfor revision, regnskab, skat, bogføring, moms, løn og økonomi'
seotamic_open_graph_title: title
seotamic_open_graph_description: meta
seotamic_twitter_title: title
seotamic_twitter_description: meta
content:
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: Kontaktoplysninger
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vejle Skatterådgivning- & Erhvervssupport'
      -
        type: hard_break
      -
        type: text
        text: 'Kristtjørnevej 27, Egeskov'
      -
        type: hard_break
      -
        type: text
        text: '7000 Fredericia'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'CVR: 16610933'
---
