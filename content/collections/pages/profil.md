---
id: a7b3a7ad-6358-4af3-81e1-c8a1f58956cd
blueprint: page
title: Profil
template: page
updated_by: 2abdf0b4-7093-453b-81b8-b442c1efabfa
updated_at: 1658322138
seotamic_title: title
seotamic_title_prepend: true
seotamic_title_append: true
seotamic_meta_description: custom
seotamic_custom_meta_description: 'Vejle Skatterådgivning- & Erhvervssupport har eksisteret i mere end 28 år. Vi har derfor stor erfaring indenfor revision, regnskab, skat, bogføring, moms, løn og økonomi'
seotamic_open_graph_title: title
seotamic_open_graph_description: meta
seotamic_twitter_title: title
seotamic_twitter_description: meta
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vi går højt op i at have tæt og personlig kontakt til vores kunder. Din virksomhed og dit regnskab er unikt og kræver derfor, at vi har sat os grundigt ind i netop din forretning. Der er altid nogen ved telefonen, som du kender, og vi kan besvare de fleste spørgsmål med det samme. Vi besøger ofte din forretning, og vi kommer derfor til at kende hinanden godt.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vi tilbyder at lave dit totale bogholderi i løbet af året, og afslutter hvert regnskabsår med en årsrapport samt indberetning af din udvidede selvangivelse eller virksomhedsregnskab.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Har du brug for kompetent hjælp til dit momsregnskab og årsregnskab, som selvfølgelig lever op til gældende lovgivning. Vi sikrer, at du får indberettet din moms korrekt, så du får det momsfradrag, du er berettiget til. Vi sikrer også, at du får udnyttet din mulighed for korrekt skattefradrag og andre fradrag! Vi er eksperter her!'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vi hjælper også med ansøgninger om alkoholbevillinger, indberetninger til virk, ansøgning om statens hjælpepakker i forbindelse med COVID-19, opstart af virksomhed, ansøgning om CVR nr. samt mange andre relaterede opgaver.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vi har et tæt samarbejde med en Statsautoriseret revisor, og laver derfor både regnskaber for enkeltmands-virksomheder, I/S, IVS og ApS. Vi holder os altid opdaterede vedr. ny lovgivning - herunder regler og ændringer i lovgivningen, der vedrører de virksomheder, vi arbejder sammen med.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: /kontakt-os
              target: null
              rel: null
        text: 'Kontakt os'
      -
        type: text
        text: ' for en personlig samtale omkring dit behov.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'Vi hjælper mange virksomheder indenfor forskellige brancher, blandt andet:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Restauranter og cafeer'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Pizzeriaer
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Håndværkere
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Behandlere
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Frisører
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Fodterapeuter
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Fotografer
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Kunstnere
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Tøjbutikker
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Skønhedssaloner
  -
    type: paragraph
    content:
      -
        type: text
        text: '… og mange flere.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'Vi udfører følgende opgaver:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Bogføring
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Indberetning af moms'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Lønregnskab hver 14. dag eller hver måned. Herunder indberetning til e-indkomst'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Kørselsregnskab
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Skatteregnskab
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Indberetning- og ændring på forskudsopgørelse'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Årsrapport
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Indberetning af udvidet selvangivelse'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Personligt regnskab'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Budgetter og likviditetsbudgetter'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Opstart af virksomhed. Herunder ansøgning om CVR nr. m.m.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Ansøgning om alkoholbevilling'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Ansættelseskontrakter
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Ansøgning om statens hjælpepakker på grund af COVID-19'
  -
    type: paragraph
    content:
      -
        type: text
        text: '… og meget andet'
---
