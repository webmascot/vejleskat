---
id: f59cfe44-83ff-48a0-9b71-970caff63986
blueprint: page
title: 'Digital bogføring'
template: page
seotamic_title: title
seotamic_title_prepend: true
seotamic_title_append: true
seotamic_meta_description: custom
seotamic_open_graph_title: title
seotamic_open_graph_description: custom
seotamic_twitter_title: title
seotamic_twitter_description: general
updated_by: 2abdf0b4-7093-453b-81b8-b442c1efabfa
updated_at: 1691427763
seotamic_custom_meta_description: |-
  Brug din tid optimalt og gør dit bogholderi digitalt. Slut med bilagsmapper der fylder, slut med print af bilag,
  slut med bilag der er blevet væk og meget mere. Du sparer både tid, energi og penge. Kontakt os, så vil vi
  fortælle dig om proceduren.
seotamic_custom_open_graph_description: |-
  Brug din tid optimalt og gør dit bogholderi digitalt. Slut med bilagsmapper der fylder, slut med print af bilag,
  slut med bilag der er blevet væk og meget mere. Du sparer både tid, energi og penge. Kontakt os, så vil vi
  fortælle dig om proceduren.
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vi tilbyder nu også digital bogføring der selvfølgelig lever op til kravene til den nye bogføringslov.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Det er nemt!'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Spar både tid og penge.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Brug din tid optimalt og gør dit bogholderi digitalt. Slut med bilagsmapper der fylder, slut med print af bilag,'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'slut med bilag der er blevet væk og meget mere. Du sparer både tid, energi og penge. '
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'statamic://entry::531caeeb-eaf3-4c44-91bb-d44c87c9bb1f'
              rel: null
              target: null
              title: null
        text: 'Kontakt os'
      -
        type: text
        text: ', så vil vi'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'fortælle dig om proceduren.'
---
