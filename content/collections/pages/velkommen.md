---
id: 464513b5-ddb4-45aa-9349-feee48d7e456
blueprint: home
title: 'Bogføring - regnskab - rådgivning'
template: home
updated_by: 2abdf0b4-7093-453b-81b8-b442c1efabfa
updated_at: 1691518932
subtitle: 'Bogføring - regnskab - rådgivning'
tekster:
  -
    overskrift: Test
    subtitle: Test
    type: tekst
    enabled: true
  -
    overskrift: 'Test 1'
    subtitle: 'Test 1'
    type: tekst
    enabled: true
slide:
  -
    title: 'Vi tilbyder digital bogføring der lever op til kravene til den nye bogføringslov<br/>kontakt os for et tilbud.'
    type: slide
    enabled: true
  -
    title: 'Vi laver dit skatteregnskab og indberetter til skat.'
    subtitle: 'Lad os klare bogføring, regnskab og revision'
    type: slide
    enabled: true
  -
    title: 'Vi klarer din bogføring, moms og løn.'
    subtitle: 'Find flere informationer her på siden'
    type: slide
    enabled: true
seotamic_title: title
seotamic_title_prepend: true
seotamic_title_append: true
seotamic_meta_description: custom
seotamic_custom_meta_description: 'Vejle Skatterådgivning- & Erhvervssupport har eksisteret i mere end 28 år. Vi har derfor stor erfaring indenfor revision, regnskab, skat, bogføring, moms, løn og økonomi.'
seotamic_open_graph_title: title
seotamic_open_graph_description: meta
seotamic_twitter_title: title
seotamic_twitter_description: meta
image: site/frontpage-new-1657553871.png
content:
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'Bogføring - revision - regnskab'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vejle Skatterådgivning- & Erhvervssupport har eksisteret i mere end 28 år. Vi har derfor stor erfaring indenfor revision, regnskab, skat, bogføring, moms, løn og økonomi. Vi hjælper dig med at lave regnskabet for din virksomhed, så du kan bruge tiden på at drive din forretning og øge din omsætning.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Vi udfører bla. følgende opgaver:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Bogføring
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Indberetning af moms'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Lønregnskab hver 14. dag eller hver måned. Herunder indberetning til e-indkomst'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Kørselsregnskab
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Skatteregnskab
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Indberetning- og ændring på forskudsopgørelse'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: Årsrapport
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Hjælp til hjemtagelse af Covid-19 hjælpepakker'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: /profil
              rel: null
              target: null
              title: null
        text: 'Læs mere om Vejle Skatterådgivning- & Erhvervssupoort på vores profil'
---
