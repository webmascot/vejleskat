const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');
require('laravel-mix-purgecss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/site.js', 'public/js')
  .copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/webfonts', false)
  .sass('resources/sass/tailwind.scss', 'public/css')
  .options({
    processCssUrls: false,
    postCss: [tailwindcss('./tailwind.config.js')],
  });

if (mix.inProduction()) {
  mix.version();
  mix.purgeCss({
    enabled: true,
    whitelistPatternsChildren: [/^content$/],
  });
}
