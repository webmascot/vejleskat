<?php

return [

    // Name of the cookie used to store the users' prefrences
    'cookie_name' => 'COOKIE_NOTICE',

    // Consent groups
    'groups' => [
        'Nødvendige' => [
            'required' => true,
            'toggle_by_default' => true,
        ],
        'Statistik' => [
            'required' => false,
            'toggle_by_default' => true,
        ],
    ],

];